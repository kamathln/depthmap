depthmap_cv is a quick script I wrote to test an idea I got when playing with stereo images on The GIMP.

# The experiment that led to this "algorithm"

* Open the left and right images of a sterwo photograph in GIMP as two seperate layers 
 * *  (choose a small one, as this requires a good performance)
* Change the layer mode of the upper layer to "difference"
* Switch to the "Move Tool" . Make sure the tool option is set to "Move layer"
* click on the image once or press the "esc" key to make sure the image window is in focus.
* Press and hold down the right arrow key, 
* as the layer moves, note how a dark "band" or shade moves in the image. 
  Also note that the distance between the x axes of the two images when the region becomes 
  dark correspond to the depth of the object.in the Z axis.

# Quick description of the script/algorithm.

* The script expects single channel grayscale images as left and right images. 
* The script then sweeps the right image over the left image (% of the width can be supplied as start and end points)
* For each frame the difference is calculated for overlapping area. This is compared with the previous difference.
* If a substantial difference or dip is found in a region, the pixels are considered as belonging to that plane.
* The generated image is a 16 bit tiff file. It may appear dark in a viewer, Don't panic.The image does have depth information that can be read by programs,
* If you really want to view the depth image, load it in GIMP, and hit menu > colors > auto > stretch contrast. Or use color > levels if you know how to use it. 
  You will have to squeeze the right peg of the input almost all the way to the left.

# Requirements tested with. May or may not work with others:

 *  Python 2.7.9
 *  opencv = 3.3.1
 *  numpy = 1.15.0
 *  tifffile = 2018.11.6 
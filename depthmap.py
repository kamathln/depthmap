#!/usr/bin/python
# The MIT License (MIT)
# Copyright (c) 2018 "Laxminarayan Kamath G A"<kamathln@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import cv2
import numpy as np
import sys
import math
import tifffile
import time
import warnings


DEBUGG = False
PROFILEE = True



def debugg(*args):
    if DEBUGG:
        sys.stderr.write("\t".join(map(str, args + ("\n",))))


def dist(x1, y1, x2, y2):
    return math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))


def oddify(num, message=""):
    if num % 2 == 0:
        return num + 1
        if message:
            debugg(message)
    else:
        return num


class Profiler(object):

    def __init__(self):
        self.lasttime = time.time()

    def show(self, message=''):
        if PROFILEE:
            t = time.time()
            sys.stderr.write("At %d:\n" % (t))
            sys.stderr.write("Elapsed %d seconds\n", t - self.lasttime)
            if message:
                debugg()
            self.lasttime = t


class DiffQueueItem(object):

    def __init__(self, img, cropx_begin, cropx_end):
        self.img = img
        self.cropx_begin = cropx_begin
        self.cropx_end = cropx_end


class DepthmapCalculator(object):

    def __init__(self,
                 left_image,
                 right_image,
                 start=-30,
                 end=30,
                 diff_thresh=2,
                 diffdiff_thresh=3,
                 window_radius=14,
                 cleanup_radius=4,
                 step=1):
        if left_image.shape != right_image.shape:
            raise(ValueError("the two input images are of different shapes"))
        if left_image.ndim > 2:
            left_image = cv2.cvtColor(left_image, cv2.COLOR_BGR2GRAY)
            right_image = cv2.cvtColor(right_image, cv2.COLOR_BGR2GRAY)
            debugg("Image shape after grayscale",
                   left_image.shape, right_image.shape)

        self.left_image = left_image.astype(None)
        debugg("left image shape", left_image.shape, self.left_image.shape)
        self.right_image = right_image.astype(None)

        self.start = start
        self.end = end
        self.step = step
        self.diff_thresh = diff_thresh
        self.diffdiff_thresh = diffdiff_thresh
        window_radius = oddify(window_radius, message="Window_radius oddified")
        self.window_radius = window_radius
        self.cleanup_radius = cleanup_radius

        if self.cleanup_radius < 0:
            self.cleanup_radius = self.window_radius / 4.0
        if self.cleanup_radius < 2:
            self.cleanup_radius = 2

        self.image_shape = self.left_image.shape[:2]
        self.image_type = self.left_image.dtype
        self.width = self.image_shape[1]
        self.height = self.image_shape[0]

        self.common_x_begin = None
        self.common_x_end = None

    @staticmethod
    def _translate_image(image, x, y):
        translation_matrix = np.float32([[1, 0, x], [0, 1, y]])
        return cv2.warpAffine(image,
                              translation_matrix,
                              (image.shape[1], image.shape[0]))

    def calc_depthmap(self):
        lastdiff = None
        depthmap = np.zeros_like(self.right_image).astype(np.uint16)

        end = int(self.width * self.end / 100.0)
        start = int(self.width * self.start / 100.0)
        location = start
        debugg("will compare from %d, %d" % (location, end))

        translated_left = self._translate_image(self.left_image, location, 0)

        def is_in_focus(diff, lastdiff):
            return int(diff < self.diff_thresh and (lastdiff - diff) > self.diffdiff_thresh)

        vec_is_in_focus = np.vectorize(is_in_focus)

        def copy_new_pixels(oldlayer, newlayer):
            return newlayer if newlayer != 0 else oldlayer

        vec_copy_new_pixels = np.vectorize(copy_new_pixels)

        while location <= end:
            debugg("Now at %d" % location)
            translated_left = self._translate_image(self.left_image,
                                                    location,
                                                    0)

            # Get area common to both images
            self.common_x_begin = 0 if location < 0 else location
            self.common_x_end = self.width if (location + self.width) > self.width else (location + self.width)

            # Make diff
            diff = np.abs(translated_left - self.right_image)
            if DEBUGG:
                cv2.imwrite('intermediate/_diff_%04d.jpg' % (location),  diff)
            tdepthmap = np.zeros_like(depthmap).astype(np.uint16)

            # Preps to compare with prev diff
            blurdiff = cv2.GaussianBlur(
                diff[:, self.common_x_begin:self.common_x_end + 1], (self.window_radius, self.window_radius), 3)
            blurdiffcur = blurdiff[:, 0:blurdiff.shape[1] - 1]

            if lastdiff is None:
                lastdiff = blurdiffcur.copy().astype(np.uint16)
                lastdiff.fill(255)
            if lastdiff.shape[1] > blurdiffcur.shape[1]:
                lastdiff = lastdiff[
                    :, lastdiff.shape[1] - blurdiffcur.shape[1]:]
                debugg("Fixed lastdiff")
            if lastdiff.shape[1] < blurdiffcur.shape[1]:
                blurdiffcur = blurdiffcur[:, :lastdiff.shape[1]]
                debugg("Fixed blurdiffcur")

            debugg(lastdiff.shape, blurdiffcur.shape)

            # Compare with previous diff
            ttdepthmap = vec_is_in_focus(blurdiffcur, lastdiff)
            tdepthmap = np.zeros(self.right_image.shape, dtype=np.uint8)
            tdepthmap[:, self.common_x_begin:
                      self.common_x_begin + ttdepthmap.shape[1]] = ttdepthmap

            debugg("max:", tdepthmap.max())

            # cleanup
            tdepthmap = cv2.medianBlur(
                tdepthmap, oddify(int(self.cleanup_radius)))

            # Merge current diff with overall depthmap
            depthmap = vec_copy_new_pixels(
                depthmap, tdepthmap.astype(np.uint16) * (location - start + 1))

            if DEBUGG:
                tdepthmap *= 255
                cv2.imwrite('intermediate/_tdepthmap_%04d.png' %
                            (location), tdepthmap)
#               tifffile.imsave('intermediate/_depthmap_%04d.tiff' % (location),
#                               depthmap)

            location += self.step
            lastdiff = blurdiff.copy()

        # Interpolation using in-painting.
        debugg(
            "Done with intermediate depthmaps. Now creating mask for inpainting")
        tmask = ((depthmap == 0) * 255)
        # edges = cv2.Canny(self.left_image.astype(np.uint8), 255, 230)
        blurred_image = cv2.medianBlur(
            self.left_image.astype(np.uint8), self.window_radius)
        edges = cv2.Sobel(blurred_image.astype(np.uint8), cv2.CV_64F,
                          1, 1, ksize=3, scale=2, delta=2.5).astype(np.uint8)
        edges = (
            (cv2.medianBlur(edges.astype(np.uint8), 3) > 5) * 255).astype(np.uint8)
        debugg('edges shape and type', edges.shape, edges.dtype)
        mask = np.abs(tmask - edges).astype(np.uint8)
        debugg("Done with mask. Now inpainting to cleanup")
        if DEBUGG:
            cv2.imwrite('intermediate/depthmap_orig.png', depthmap)
            cv2.imwrite('intermediate/mask.png', tmask)
            cv2.imwrite('intermediate/edges.png', edges)
        depthmap = cv2.inpaint(
            depthmap, tmask.astype(np.uint8), 1, cv2.INPAINT_TELEA)
        depthmap = cv2.inpaint(depthmap, edges, 1, cv2.INPAINT_TELEA)
        debugg("Done")
        return depthmap

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser('depthmap')
    ap.add_argument('left_image', help='image from left camera: single channel grayscale image')
    ap.add_argument('right_image', help='image from right camera: single channel grayscale image')
    ap.add_argument('output_image', help='output image filename WARNING: This will always be a 16bit tiff file')
    ap.add_argument('--start', type=int, default=-10, action='store', help='start of the sweep of right image on left image for comparison in terms of percentage of width. Can be negative')
    ap.add_argument('--end', type=int, default=30, action='store', help='end of the sweep')
    ap.add_argument('--step', type=int, default=1, action='store', help='steps in number of pixels. Will affect the difference between prev and current comparisons')
    ap.add_argument('--window_radius', type=int, default=5, action='store', help='window radius to convolute the difference to remove noise')
    ap.add_argument('--cleanup_radius', type=int, default=7, action='store', help='radius to convolute the intermediate depthmap to cleanup')
    ap.add_argument('--diff_thresh', type=int, default=16, action='store', help='threshold of difference below which to consider a pixel as at current distance')
    ap.add_argument('--diffdiff_thresh', type=int, default=4, action='store', help='threshold of difference between curr diff and prev above which to consider a pixel as at current distance')
    ap.add_argument('--debug', dest='DEBUG', action='store_true', help="""get lots of debugging outputs. Most will be meaningless to people who haven't seen the code. 
WARNING: expects a folder called "intermediate", and will output lots of images to the folder for debugging purposes""")
    options = ap.parse_args(sys.argv[1:])
    DEBUGG = options.DEBUG
    debugg("final options after parsing:", options.__dict__)

    debugg("Will write to target: %s" % options.output_image)
    left_image = cv2.imread(options.left_image, -1)
    right_image = cv2.imread(options.right_image, -1)

    depther = DepthmapCalculator(
        left_image,
        right_image,
        start=options.start,
        end=options.end,
        step=options.step,
        diff_thresh=options.diff_thresh,
        cleanup_radius=options.cleanup_radius,
        diffdiff_thresh=options.diffdiff_thresh,
        window_radius=options.window_radius)
#    depthmap = depther.calc_depthmap()
    depthmap = depther.calc_depthmap().astype(np.uint16)
    tifffile.imsave(options.output_image, depthmap)
